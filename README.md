![logo](https://gitlab.com/F.Woldmann/cansat-argus-saxonia/-/raw/images/logo.png)

# CanSat Satellite Argus Saxonia

As part of the german [CanSat](https://www.cansat.de/) competition we are designing a can sized satellite to be propelled to a height of about one kilometer. From there the satellite will be measuring a wide range of conditions. The main goal of this mission is to capture footage of the ground to be analysed.

This repository mainly holds the code which will enable our satellite to carry out this mission as well as some technical information about the satellite.

## 1. Circuit Diagram

![circuit diagram](https://gitlab.com/F.Woldmann/cansat-argus-saxonia/-/raw/images/circuitDiagram.png)

1. [Raspberry Pi](#raspberry-pi)
2. [Digital pressure sensor BMP180](#digital-pressure-sensor-bmp180)
3. [BNO055 9-DOF IMU](#digital-imu-bosch-bno055)
4. [SIM868 GPS/GSM-Module](#gps-and-gsm-module-sim868)
5. [Lithium Ion battery](#lithium-ion-battery)
6. [Battery management system](#battery-management-system)
7. [Power plug](#power-plug)
8. [Step-down converter](#step-down-converter)
9. [Raspberry Pi Camera V2](#raspberry-pi-camera-v2)

### Raspberry Pi

The core of our satellite is a Raspberry Pi 3. The Pi is managing all the sensors like the BMP180 or the BNO055 as well as the camera
and the EXIF-Tool. Of course we cannot change the program on the flight so we must prepare everything very carefully. 
Therefore it is vital to make sure that the Python code we create is working properly and without any issues.

A proper Documentation on the Raspberry Pi can be found here:
[Raspberry Pi 3 documentation](https://github.com/raspberrypi/documentation)

A datasheet can be found here: 
[Raspberry Pi 3 datasheet](https://components101.com/sites/default/files/component_datasheet/Raspberry%20Pi%203%20Datasheet.pdf)


### Digital pressure sensor `BMP180`

[Data sheet](https://cdn-shop.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf)

The BMP180 is connected to the raspberry pi via an SDA and SCL pin. Communication utilizes the I2C protocol. Apart from that it requires 3.3V as well as a ground connection.

Naturally we are not writing all the code from scratch for a sensor like this. To make interacting with the sensor easier we are using [Adafruit's Raspberry-Pi Python Code Library](https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/tree/legacy).

**provided example**

```python
from modules.Adafruit_BMP085 import BMP085
```

Initialise the BMP085 and use STANDARD mode (default value)

```python
# bmp = BMP085(0x77, debug=True)
bmp = BMP085(0x77)
```

To specify a different operating mode:

```python
bmp = BMP085(0x77, 0)  # ULTRALOWPOWER Mode
bmp = BMP085(0x77, 1)  # STANDARD Mode
bmp = BMP085(0x77, 2)  # HIRES Mode
bmp = BMP085(0x77, 3)  # ULTRAHIRES Mode
```

Read the current temperature

```python
temp = bmp.readTemperature()
```

Read the current barometric pressure level

```python
pressure = bmp.readPressure()
```

To calculate altitude based on an estimated mean sea level pressure (1013.25 hPa) call the function as follows, but this won't be very accurate.

```python
altitude = bmp.readAltitude()
```

To specify a more accurate altitude, enter the correct mean sea level
pressure level. For example, if the current pressure level is 1023.50 hPa
enter 102350 since we include two decimal places in the integer value

```python
altitude = bmp.readAltitude(102350)
```

### Digital IMU Bosch `BNO055`

The digital IMU BNO055 is a 9-DOF Sensor made by the German company Bosch.
The data output includes: absolute orientation (Euler angle as well as Quaternion), 
angular velocity vector, acceleration vector, Geo-magnetic field vector, 
linear acceleration vector, gravity vector and the ambient temperature.
Using the I2C connection interface via SDA and SCL to communicate the collected data to our Raspberry Pi, 
it is very easy to install and run. The only other two wires needed are a 3.3V PIN and a GND PIN.

A full documentation of the sensor can be found here:
[BNO055 Documentation](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor?view=all)

Due to the fact that the sensor is very versatile to use, there are hundreds of code examples on the internet. We used:
[Adafruit BNO055 Python library](https://github.com/adafruit/Adafruit_CircuitPython_BNO055) 

**provided example**

```python
import adafruit_bno055
```

Adafruit offers a wide variety of code examples, not only for Raspberry Pi projects. To address the sensor, they use their own syntax.

```python
sensor = adafruit_bno055.BNO055_I2C(i2c)
```

The Adafruit library is vast and offered us the possibility to simply grap the output of their code with one simple command.

```python
print(sensor.temperature)
print(sensor.euler)
print(sensor.gravity)
```
Therefore we are able to manage the entire BNO055 IMU with little effort which is of great use for our project. 

### GPS and GSM Module `SIM868`

The SIM868 is a commonly used device for GPS Data collection. Besides that, as the name indicates, it is also a GSM module,
therefore you can use it to perform calls or even send SMS messages from your Raspberry Pi.
It´s compact size makes it great for our CanSat. It is connected through a RX/TX serial connection.  
The other notable PINs are a Power (or Wake-Up if you want) PIN, used for setting up the module. It works with a 5V voltage,
and of course you need a GND connection.

A documentation of the module can be found here: 
[SIM868 Documentation](https://docs.rs-online.com/2190/0900766b815ca94b.pdf)

The way of using of this SIM868 module is more complex than the first two sensors. It is a modem using AT commands. 
These must be sent using the serial connection of the Raspberry Pi. This seems easy, but these commands are a bit complicated.
First of all, the GPS module recieves several packages of GPS data sent by the different satellite systems orbiting the earth. 
Those include the American GPS, the Russian GloNas, the European Galileo and the Chinese Beidou satellite systems. 
The SIM868 provides us with a NMEA data structure which contains the received positioning information.
From among them we are using the GNGGA, GNRMC, GNVTG sentences.

The received GNSS values are stored in a structure:

```python
GNSS_Data = {'year': 0,
             'month': 0,
             'day': 0,
             'hour': 0,
             'minute': 0,
             'second': 0,
             'nano': 0,
             'latitude_num': 0.0,
             'latitude': 'some val',
             'latitude_dir': 'North',
             'longitude_num': 0.0,
             'longitude': 'some val',
             'longitude_dir': 'East',
             'altitude': 0.0,
             'altitude_units': 'M',
             'spd_over_grnd_kmph': 0.0,
             'num_sats': 0,
             'gps_qual': 0,
             'true_course': 0.0,
             'true_track': 0.0, # geographic track
             'mag_track': 0.0,  # magnetic track
             'mag_variation': 'some val',
             'mag_var_dir': 'some val',}
```

All these values are updated each second. To read the data (for example the altitude) queued on the serial interface we use the python command:
 
```python
msg = pynmea2.parse(line)
```

This does put the message stored in `line`, containing one single complete NMEA sentence, into a parser. 
This is used to decode the information stored in the NMEA sentences of the SIM868.
After that, the message will be stored in our structure.

```python
GNSS_Data['hour'] = '%02d' % msg.timestamp.hour
GNSS_Data['minute'] = '%02d' % msg.timestamp.minute
GNSS_Data['second'] = '%02d' % msg.timestamp.second
GNSS_Data['latitude_num'] = msg.latitude
GNSS_Data['latitude'] = '%02d°%02d\'%07.4f\"' % (msg.latitude, msg.latitude_minutes, msg.latitude_seconds)
if(msg.lat_dir == 'N'):
   GNSS_Data['latitude_dir'] = 'North'
if (msg.lat_dir == 'S'):
    GNSS_Data['latitude_dir'] = 'South'
GNSS_Data['longitude_num'] = msg.longitude
GNSS_Data['longitude'] = '%03d°%02d\'%07.4f\"' % (msg.longitude, msg.longitude_minutes, msg.longitude_seconds)
if (msg.lon_dir == 'E'):
    GNSS_Data['longitude_dir'] = 'East'
if (msg.lon_dir == 'W'):
    GNSS_Data['longitude_dir'] = 'West'
GNSS_Data['altitude'] = msg.altitude
GNSS_Data['altitude_units'] = msg.altitude_units
GNSS_Data['num_sats'] = msg.num_sats
GNSS_Data['gps_qual'] = msg.gps_qual
```

Also we need to make sure that these values are not being written and read at the same time. 
That´s why we use a `lock` containing all these statements.

```python
GNSS_Lock.acquire()
...
GNSS_Lock.release()
``` 
With the data saved in our structure they are ready to be used elsewhere, such as the EXIFTool which writes all these information 
into the metadata of the pictures taken by the camera.

### Lithium Ion battery

Obviously the satellite must be supplied with power. Therefore we installed 3 Lithium Ion battery packs of the type 18650 in our CanSat.
Each of those has a capacity of 3000mAh, adding up to 9000mAh with all three of them. This amount of available energy enables the 
satellite to run for round about 4-5 hours. The most energy will be claimed by the Pi of course, but the camera and especially the SIM868 
module take a lot of energy as well.

A documentation about the batteries can be found here:
[Li-Ion 18650 batteries documentation](https://www.ineltro.ch/media/downloads/SAAItem/45/45958/36e3e7f3-2049-4adb-a2a7-79c654d92915.pdf) 

### Battery management system

For the sake of loading our batteries we had to include a battery management system (BMS). The main reason for this is the wiring of the 
three batteries. Because we connected them to be in a series connection, loading the batteries isnt that easy. 
The BMS basically steers the amount of energy put in each of the battery packs and protects them from overloading.

For more detailled information, check out this:
[Battery management system documentation](https://www.instructables.com/DIY-Professional-18650-Battery-Pack/)

###  Power plug

This is the power plug used to charge the Li-Ion batteries. No further information needed.

### Step-down converter

Our batteries deliver a voltage in the range of 3.7V to 4.2V each. Combined this adds up to a total voltage of around 12V. 
This is to high for our Raspberry Pi to work. Therefore we use this step-down converter to efficiently reduce the
voltage to the needed 5.2V. As described above, some of the sensors use 3.3V instead of the 5V. 
Thats why the Raspberry Pi has an internal voltage adaption system, allowing him to provide the 
voltage given to the sensors. All the sensors operating with 3.3V are connected to the 3.3V output of the 
Raspberry Pi. This can be seen in the circuit diagram.

Further information about step-down converters in general can be found here:
[Step-down converter information](https://en.wikipedia.org/wiki/Buck_converter)

The model we used for our project is a XL4015 DC-DC converter.
It is quite small resulting in great versatility and usability for our project.
You can find the model here:
[XL4015 DC-DC converter](https://www.az-delivery.de/collections/basis-produkte/products/spannungswandler-5a-8-36v-zu-1-25-32v)

### Raspberry Pi camera V2

The Raspberry Pi camera V2 (or PiCam V2) is a small but excellent camera module. Its compact form and high resolution makes it 
fit our requirements perfectly. 

The camera is used via a simple OS command. A full documentation about the camera can be found here:
[Raspberry Pi camera V2](https://www.raspberrypi.org/documentation/hardware/camera/)

---
